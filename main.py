import requests

API_KEY = "b852e84019a2f9a21e0498406098450f"

headers = {
    "x-apisports-key": API_KEY
}

base_url = "https://v3.football.api-sports.io/"


## RICHIESTA STATUS
endpoint = "status"
url = f"{base_url}{endpoint}" ##creo l'url che mi serve con base e endpoint dinamico
print(url)

response = requests.request("GET", url, headers = headers) ##indico il tipo di chiamata HTTP, verso quale url, trasportando i dati delle chiavi api
print(response.status_code)
print(response.text)
dictionary = response.json()
import json
print(json.dumps(dictionary, indent = 4))


## RICHIESTA EFFETTIVA
endpoint = "countries"
url = f"{base_url}{endpoint}"
countries = requests.request("GET", url, headers = headers)
# print(countries.json())
countries_dict = countries.json()['response'] ##recupero solo la chiave response del JSON
# print(len(countries_dict)) ##conto quante nazioni ci sono presenti nel "DB"
# print(json.dumps(countries_dict[0], indent = 4)) ##vedo i dati della prima nazione in lista


## RICHIESTA RELATIVA ALL'ITALIA
##mi creo una lista di parametri che mi interessa per cercarli nel dict
params = {
    'code' : 'IT'
}
italy = requests.request("GET", url, headers = headers, params = params)
# print(italy.json()) ##nel risultato della request vediamo anche i dati passati, più la chiave response che è quella che interessa a noi
# print(italy.request.url) ##vediamo effettivamente l'url lanciato dal nostro programma


## RICHIESTA RELATIVA ALLA SERIE A
endpoint = "leagues"
url = f"{base_url}{endpoint}"

params = {
    'code' : 'IT'
}
leagues = requests.request("GET", url, headers = headers, params = params)
# print(json.dumps(leagues.json()['response'], indent = 4)) ##tutte le leghe e i trofei degli ultimo 10 anni
italy_leagues = leagues.json()['response'] ##mi salvo tutte le leghe
# print(len(italy_leagues))
serie_a = italy_leagues[0] ##mi salvo solo la Serie A
# print(serie_a['league'])
serie_a_id = serie_a['league']['id'] ##mi salvo l'id della Serie A
# print(serie_a_id)


## RICHIESTA RELATIVA ALLA STAGIONE IN CORSO
params = {
    'id' : serie_a_id,
    'season' : 2022
}
current_season = requests.request("GET", url, headers = headers, params = params)
# print(json.dumps(current_season.json(), indent = 2))


## RICHIESTA RELATIVA A UNA SQUADRA SPECIFICA
endpoint = "teams"
url = f"{base_url}{endpoint}"

params = {
    'league' : serie_a_id,
    'season' : 2022
}

teams = requests.request("GET", url, headers = headers, params = params)
teams_list = teams.json()['response'] ##mi salvo la lista delle squadre
for team in teams_list: ##creo una visualizzazione più comprensibile chiave/valore
    print("LISTA ORDINATA")
    for key, value in team['team'].items():
        print(f"{key}: {value}") ##stampo i dati della squadra
    for key, value in team['vanue'].items():
        print(f"{key}: {value}") ##stampo i dati dello stadio
    
    print("\n")